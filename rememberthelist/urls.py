from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',


    url(r'^(?P<list_key>[a-zA-Z0-9]{62})$', views.show_list, name='show_list'),
   # url(r'^new_list$', views.new_list, name='new_list'),
    url(r'^new_item/(?P<list_key>[a-zA-Z0-9]{62})$', views.new_item, name='new_item'),
    url(r'^delete_item$', views.delete_item, name='delete_item') ,
    url(r'^update_item_pos$', views.update_item_pos, name='update_item_pos') ,
    url(r'^check_item$', views.check_item, name='check_item') ,
    url(r'^update_text_item$', views.update_text_item, name='update_text_item') ,
    url(r'^$', views.lists, name='home'),
    #url(r'^', views.lists , name='homes'),
)
