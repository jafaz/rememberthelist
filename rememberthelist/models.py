from django.db.models import Model, SlugField, CharField, DateTimeField, BooleanField, PositiveIntegerField, ForeignKey, permalink, FloatField


class List(Model):
    key = SlugField(max_length=62, unique=True)   # All list keys are 62 chars long
    name = CharField(max_length=140, blank=False)
    creation_date = DateTimeField(auto_now=True)
    last_modification_date = DateTimeField(auto_now=True)


    def __unicode__(self):
        return self.name


    @permalink
    def get_absolute_url(self):
        return ('show_list', None, {'list_key': self.key})

    # def get_absolute_url(self):
    #     return "/%s" % self.key


class Item(Model):
    text = CharField(max_length=140, blank=False)
    checked = BooleanField(default=False)
    position = FloatField(blank=True)
    lst = ForeignKey(List)



    #nextpost =  model.objects.order_by('-position')[0].position + 1
    #self.objects.filter(lst=una).order_by('-position')[0].position
    # model = self.__class__

    def save(self, *args, **kwargs):

        if ( self.position == None ):
            lst = self.lst
            items = Item.objects.filter(lst=lst).order_by('-position')
            if ( len(items) == 0 ):
                self.position = 1
            else:
                self.position = items[0].position + 1

        return super(Item, self).save(*args, **kwargs)


    def __unicode__(self):
        return self.text

    # class Meta:
    #     order_with_respect_to = 'list'
    #     #ordering = ['number']
    #     #unique_together = ('list', 'number')
