from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import List, Item

class ListForm(forms.ModelForm):
    name = forms.CharField(
        label='',
    )
    class Meta:
        model = List
        fields = ('key', 'name',)
        widgets = {'key': forms.HiddenInput()}

class ItemForm(forms.ModelForm):
    text = forms.CharField(
        label='New Item',
        widget=forms.TextInput(),
    )

    class Meta:
        model = Item
        fields = ('text','lst')
        widgets = {'lst': forms.HiddenInput(), 'position':forms.HiddenInput()}


class EditItemForm(forms.ModelForm):
    text = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={'class': "item-text hidden-text"}),
    )
    checked = forms.BooleanField(
        label='',
        widget=forms.CheckboxInput(attrs={'class': "item-checked"}),
    )


    class Meta:
        model = Item
        fields = ('id', 'text', 'lst', 'checked','position')
        widgets = {'lst': forms.HiddenInput(), 'position':forms.HiddenInput()}
