from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from .models import List, Item
from .forms import ListForm, ItemForm, EditItemForm
from .auxiliar import *
import json
import string
from random import choice

LIST_KEY_CHARS = string.ascii_letters + string.digits
#ist_key = "".join([choice(LIST_KEY_CHARS) for x in range(62)])

# def if_list_fun(_id, key, function):
#     lst = List.objects.get(id=_id)
#     if ( lst.key == key ):
#         function()



# def new_list(request):
#     if request.method == 'POST':
#         return HttpResponse(json.dumps({'message': "hola soy post"}))
#     return HttpResponse(json.dumps({'message': "hola soy get"  }))



def lists(request):
    key = "".join([choice(LIST_KEY_CHARS) for x in range(62)])
    form = ListForm(initial={'key':key, })

    if request.method == 'POST':
        form = ListForm(request.POST)

        if form.is_valid():
            lst = form.save()
            return HttpResponseRedirect(reverse('show_list',  kwargs={'list_key': lst.key}))

    lists = List.objects.all()
    return render(request, 'lists.html', {'lists': lists, 'form':form})

def new_item(request, list_key):
    if request.method == 'POST':
        lst = List.objects.get(id=request.POST.get('lst'))
        if ( lst.key == list_key ):
            form = ItemForm(request.POST)
            #return HttpResponse(json.dumps({'message': "hola post", "post": request.POST}))
            if form.is_valid():
                item = form.save()
                ritemForm = EditItemForm(initial={'lst':lst, })

                return HttpResponse(json.dumps({'message': 'ok',
                                                'id':item.id,
                                                'text':item.text,
                                                'position': item.position,
                                            }))
    return redirect('home')

def delete_item(request):
    if request.method == 'POST':
        key = request.POST.get('list_key')
        lst = List.objects.get(id=request.POST.get('list_id'))

        if ( lst.key == key ):
            item_id = request.POST.get('item_id')
            item = Item.objects.get(id=item_id)
            item.delete()
            return HttpResponse(json.dumps({'message': "hola soy post de delete"  }))
    return redirect('home')

def update_item_pos(request):
    if request.method == 'POST':
        key = request.POST.get('list_key')
        lst = List.objects.get(id=request.POST.get('list_id'))
        pos = request.POST.get('item_pos')

        #TODO validate number
        if ( lst.key == key ):
            item_id = request.POST.get('item_id')
            item = Item.objects.get(id=item_id)
            item.position = pos
            item.save()
            #item.update(position=pos)
            return HttpResponse(json.dumps({'message': "hola soy post de update" +   pos + item_id  }))

    return redirect('home')


def check_item(request):
    if request.method == 'POST':
        key = request.POST.get('list_key')
        lst = List.objects.get(id=request.POST.get('list_id'))

        # #TODO validate number
        if ( lst.key == key ):
            item_id = request.POST.get('item_id')
            item = Item.objects.get(id=item_id)
            item.checked = not item.checked
            item.save()

            return HttpResponse(json.dumps({'message': "hola soy post de check"   }))

    return redirect('home')


def update_text_item(request):
    if request.method == 'POST':
        key = request.POST.get('list_key')
        lst = List.objects.get(id=request.POST.get('list_id'))

        if ( lst.key == key ):
             item_id = request.POST.get('item_id')
             item = Item.objects.get(id=item_id)
             item.text = request.POST.get('item_text')
             item.save()
             return HttpResponse(json.dumps({'message': "hola soy update item"  }))

    return redirect('home')



def show_list(request, list_key):

    #return redirect('home')
    try:
        lst = List.objects.get(key=list_key)
    except:
        return redirect('home')

    form = ItemForm(initial={'lst':lst, })
    if request.method == 'POST':
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()

    items = Item.objects.filter(lst=lst).order_by('position')

    def makeItemForm(item):
        return  EditItemForm(instance=item)

    itemForms = list(map(makeItemForm, items))

    return render(request, 'list.html', {
        'list':lst,
        'form' : form,
        'itemForms': itemForms,
        'tmp' : list_key,
    })
